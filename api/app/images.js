const express = require('express');
const axios = require('axios');
const path = require('path');
const multer = require('multer');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Image = require('../models/Image');

const config = require('../config');
const nanoid = require("nanoid");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/',(req,res) =>{
    const criteria  = {};
    if (req.query.place) {
        criteria.place = req.query.place;
    }
    Image.find(criteria)
         .then(result=>res.send(result))
         .catch(() => res.sendStatus(500))
});

router.post('/', [auth, upload.single('image')], (req,res) => {
    const imageData = req.body;

    if (req.file) {
         imageData.image = req.file.filename;
    }

    imageData.user = req.user._id;

    const image = new Image(imageData);

    image.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});
router.delete('/:id', [auth,permit('admin')],(req,res)=>{
    Image.findByIdAndDelete({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error=>res.status(403).send(error))

});

module.exports = router;