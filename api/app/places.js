const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const tryAuth = require('../middleware/tryAuth');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Place = require('../models/Place');
const config = require("../config");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/',tryAuth, (req,res) =>{
   Place.find()
       .then(result=>res.send(result))
       .catch(() => res.sendStatus(500))
});

router.get('/:id',async (req,res)=>{
    try {
        const place = await Place.findById(req.params.id);
        return res.send(place)
    }catch (e) {
        return res.status(500).send(e)
    }

});

router.post('/',[auth,upload.single('logo')],(req,res)=>{
    const placeData = req.body;
    placeData.owner = req.user._id;

    if(placeData.agreeChecked !== 'true'){
        return res.status(400).send({message:'You need to agree with rule of the site before post!'})
    }
    if (req.file) {
        placeData.logo = req.file.filename;
    }
    const place = new Place(placeData);
    place.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.delete('/:id', [auth,permit('admin')],(req,res)=>{
    Place.findByIdAndDelete({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error=>res.status(403).send(error))

});

module.exports = router;