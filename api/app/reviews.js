const express = require('express');
const axios = require('axios');
const tryAuth = require('../middleware/tryAuth');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Review = require('../models/Review');

const router = express.Router();

router.get('/',tryAuth, (req,res) =>{
    const criteria  = {};
    if (req.query.place) {
        criteria.place = req.query.place;
    }
   Review.find(criteria).populate('user')
       .then(result=>res.send(result))
       .catch(() => res.sendStatus(500))
});

router.post('/',[auth],(req,res)=>{
    const reviewData = req.body;
    reviewData.user = req.user._id;
    reviewData.date = new Date();

    const review = new Review(reviewData);
    review.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.delete('/:id', [auth,permit('admin')],(req,res)=>{
    Review.findByIdAndDelete({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error=>res.status(403).send(error))

});

module.exports = router;