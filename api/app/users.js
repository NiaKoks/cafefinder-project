const express = require('express');
const axios = require('axios');
const nanoid = require('nanoid');

const multer = require('multer');
const path = require('path');
const config = require('../config');
const User = require('../models/User');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.post('/',  upload.single('avatar'), async(req, res) => {

    try {
        const userData = req.body;

        if (req.file) {
            userData.avatar = req.file.filename
        }

        const user = await User(userData);

        user.generateToken();

        await user.save();
        return res.send({message: 'User registered', user});
    } catch (error) {
        return res.status(400).send(error)
    }
});

router.post('/facebookLogin', async (req, res) => {

    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        const responseData = response.data;


        if (responseData.data.error) {
            return res.status(500).send({error: "Неверный токен"});
        }

        if (responseData.data.user_id !== req.body.id) {
            return res.status(500).send({error: 'Пользователь не существует'});
        }

        let user = await User.findOne({email: req.body.email});


        if (!user) {
            user = new User({
                email: req.body.email || req.body.id,
                password: nanoid(),
                facebookId: req.body.id,
                avatar: req.body.picture.data.url,
                displayName: req.body.name
            })
        }

        user.generateToken();

        await user.save();

        return res.send({message: 'Login or register successful!', user});

    } catch (e) {
        return res.status(500).send({error: 'Что-то пошло не так'});
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({email: req.body.email});

    if (!user) {
        return res.status(401).send({error: 'Пользователь не существует'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(401).send({error: 'Неверный пароль'});
    }

    user.generateToken();

    await user.save();

    res.send({message: 'Login successful', user});
});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Logged out'};

    if(!token) {
        return res.send(success);
    }
    const user = await User.findOne({token});

    if(!user) {
        return res.send(success);
    }

    user.generateToken();
    await user.save();

    return res.send(success);
});


module.exports = router;
