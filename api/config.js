const path = require('path');

const rootPath = __dirname;

const dbUrl = process.env.NODE_ENV === 'test' ? 'mongodb://localhost/cafe_test': 'mongodb://localhost/cafe';

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl,
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '1129433940573928',//изменить для регистрации через fb
    appSecret: '567829af0c08b2c4e7638cb038315736' // insecure! изменить для регистрации через fb
  }
};
