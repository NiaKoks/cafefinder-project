const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const User = require('./models/User');
    const Place = require('./models/Place');
    const Image = require('./models/Image');
    const Review = require('./models/Review');

    const user = await User.create(
        {
            email: 'JustUser@gmail.com',
            password: '123',
            token: nanoid(),
            displayName: 'User',
            avatar: 'john-wick.jpg',
        },
        {
            email: 'MoreThanUser.AdminBabe@gmail.com',
            password: '123',
            token: nanoid(),
            displayName: 'Admin',
            avatar: 'admin.jpg',
            role: 'admin'
        },
        { email:'saviour@mail.com',
          password:'123',
          token:nanoid(),
          displayName: 'Shepard',
          avatar: null,
          role:'user'
        },
    );

    const place = await Place.create(
        { name:'Lady Marry',
          description:'Cozy place',
          owner: user[1],
          logo: 'marrypoppins.jpg'
        },
    );

    await Review.create(
        {
            text:'Amazing place',
            date:'2019-08-17',
            user: user[0],
            place: place,
            foodRating: 5,
            serviceRating:3,
            interiorRating:5
        },
        {
            text:'This is my favourite cafe on Citadel',
            date:'2019-08-17',
            user: user[2],
            place: place,
            foodRating: 5,
            serviceRating:5,
            interiorRating:5
        },
        {
            text:'Food was tasty, interior looking nice enough but service was just fucking atrocious',
            date:'2019-08-17',
            user: user[1],
            place: place,
            foodRating: 5,
            serviceRating:4,
            interiorRating:1
        },
    );

    await Image.create(
        {
          image:'interior.jpg',
          place: place,
          user: user[0]
        },
        {
          image:'interior2.jpg',
          place: place,
          user: user[2]
        }
    );

    return connection.close();
};

run().catch(error => {
    console.error('Something wrong happened...', error);
});
