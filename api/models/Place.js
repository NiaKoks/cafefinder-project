const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PlaceSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    description: {
        type: String,
        required: true
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    logo: String,
});

const Place = mongoose.model('Place', PlaceSchema);

module.exports = Place;