const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    text:{
        type:String,
        required:true
    },
    date:{
      type: Date,
      required:true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    place:{
      type: Schema.Types.ObjectId,
      ref: 'Place',
      required: true
    },
    foodRating: {
      type: Number,
      required: true
    },
    serviceRating: {
      type: Number,
      required: true
    },
    interiorRating: {
      type: Number,
      required: true
    },

});

const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;