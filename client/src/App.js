import React, {Component, Fragment} from 'react';
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";

import {logoutUser} from "./store/actions/usersActions";
import './App.css';

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Routes from "./Routes";
import Footer from "./components/UI/Footer/Footer";


class App extends Component {
  render() {
    return (
        <Fragment>
          <NotificationContainer/>
          <header>

            <Toolbar
                user={this.props.user}
                logout={this.props.logoutUser}
            >

            </Toolbar>
          </header>

          <Routes
              place={this.props.place}
              user={this.props.user}/>
          <footer>
            <Footer
                user={this.props.user}
            />
          </footer>
        </Fragment>
    );
  }
}


const mapStateToProps = state => ({
  user: state.users.user,
  place: state.places.singlePlace
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
