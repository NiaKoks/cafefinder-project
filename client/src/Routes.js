import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Places from "./containers/Places/Places"
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewPlace from "./containers/NewPlace/NewPlace";
import PlacePage from "./containers/PlacePage/PlacePage";

const ProtectedRoute = ({isAllowed, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to="/login" />
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={Places}/>
            <ProtectedRoute
                isAllowed={(user && user.role === 'admin') || (user && user.role === 'user')}
                path="/places/create_new"
                exact component={NewPlace}
            />
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/places/:id" exact component={PlacePage}/>

        </Switch>
    );
};

export default Routes;