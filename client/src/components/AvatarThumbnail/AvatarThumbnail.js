import React from 'react';
import config from '../../config'

import avatarnNotAvailable from '../../assets/images/avatar_not _available.png';

const styles = {
    width: '50px',
    height: '50px',
    borderRadius: '50%',
    display: 'inline-block'
};

const ProductThumbnail = (props) => {

    const src = props.user.facebookId ? props.user.avatar : config.apiURL +  '/uploads/' + props.user.avatar;

    let image = avatarnNotAvailable;

    if (props.user.avatar) {
        image =  src
    }


    return <img src={image} style={styles} className="img-thumbnail" alt="avatar"/>;
};

export default ProductThumbnail;
