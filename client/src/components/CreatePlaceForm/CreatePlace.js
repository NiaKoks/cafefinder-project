import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";
class CreatePlace extends Component {

    state={
      name: '',
      description: '',
      logo:'',
      agreeChecked: false
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = event =>{
        event.preventDefault();

        if(!this.state.agreeChecked) return;

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        this.props.onSubmit(formData);
    };

    checkboxHandler = event =>{
       this.setState({agreeChecked: event.target.checked})
    };
    render() {
        return (
            <Fragment>
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="name"
                        title='Place name:'
                        type="text"
                        value={this.state.name}
                        onChange={this.inputChangeHandler}
                        placeholder="Type name of the place here"
                        required

                    />

                    <FormElement
                        propertyName="description"
                        title="Describe the place:"
                        type="textarea"
                        value={this.state.description}
                        onChange={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="logo"
                        title="Logo of the place:"
                        type="file"
                        onChange={this.fileChangeHandler}
                        required
                    />

                    <FormGroup check>
                        <Label>
                            <Input type="checkbox" onChange={this.checkboxHandler}/>  I have read user contract and agree
                        </Label>
                    </FormGroup>
                        <FormGroup>
                        <Col>
                            {this.state.agreeChecked &&
                            <Button type="submit" color="primary">Add</Button>
                            }
                        </Col>
                        </FormGroup>
                </Form>
            </Fragment>
        );
    }
}


export default  CreatePlace;