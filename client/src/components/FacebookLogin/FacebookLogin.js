import React, {Component} from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props'
import {NotificationManager} from 'react-notifications'
import {Button} from "reactstrap";
import {facebookLogin} from "../../store/actions/usersActions";
import {connect} from "react-redux";

class FacebookLogin extends Component {

    facebookLogin = data => {
        if (data.error) {
            NotificationManager.error('Что-то пошло не так')
        } else if(!data.name){
            NotificationManager.warning('Вы нажали отмена');
        } else {
            this.props.facebookLogin(data)
        }
    };
    render() {
        return (
            <FacebookLoginButton
                appId="390672758455990"
                callback={this.facebookLogin}
                fields="name, email, picture"
                render={renderProps => (
                    <Button outline color="primary" onClick={renderProps.onClick}>Войти через фейсбук</Button>
                )}
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    facebookLogin: userData => dispatch(facebookLogin(userData))
});

export default connect(null, mapDispatchToProps)(FacebookLogin);