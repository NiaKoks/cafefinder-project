import React from 'react';
import {
    Button,
    Card,
    CardBody,
    CardImg,
    CardText,
    CardTitle,
    Col,
    Row,
} from "reactstrap";
import config from "../../config";
import {Link} from "react-router-dom";
import EventDropdown from "../UI/EventDropdown";

const PlaceCard = ({user,place}) => {
    return (
        <div key={place._id}>
           <Card style={{width:'50%'}}>
               <CardBody>
                 <Row>
                     {(user && user.role === 'admin') &&
                     <EventDropdown place={place} user={user}/>
                     }
                     <Col sm={6}>
                         <CardImg src={config.apiURL + '/uploads/' + place.logo}
                                  alt="place pic"
                                  className="place-Image"
                         />
                     </Col>
                   <Col sm={10}>

                       <CardTitle className="place-Title">
                           <h2>{place.name}</h2>
                       </CardTitle>
                       <CardText className="place-CardText">
                           {place.description}
                       </CardText>
                   </Col>
                 </Row>
                   <Link to={"/places/" + place._id}>
                       <Button color="success">
                          <p>Check out place</p>
                       </Button>
                   </Link>
               </CardBody>
           </Card>
        </div>
    );
};

export default PlaceCard;