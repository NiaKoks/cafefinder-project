import React, {Component, Fragment} from 'react';
import {FaBars} from "react-icons/fa";
import {
    Button,
    DropdownItem,
    DropdownMenu,
    DropdownToggle, Modal, ModalBody, ModalFooter, ModalHeader,
    UncontrolledButtonDropdown
} from "reactstrap";
import {NavLink as RouterNavLink, withRouter} from "react-router-dom";
import {deletePlace} from "../../store/actions/placesActions";
import {connect} from "react-redux";

class EventDropdown extends Component {
    state = {
        showDeleteModal: false
    };

    toggleDeleteModal = () => {
        this.setState({showDeleteModal: !this.state.showDeleteModal});
    };

    render() {
        const {place, user} = this.props;

        return (
            <Fragment>
                <div className="Event-dropdown-btn">
                    <UncontrolledButtonDropdown>
                        <DropdownToggle caret>
                            <FaBars/>
                        </DropdownToggle>
                        <DropdownMenu right>
                            {(user._id === place.owner._id) &&
                            <DropdownItem
                                tag={RouterNavLink}
                                exact
                                to={`/places/${place._id}/edit`}
                            >
                               <p>Edit</p>
                            </DropdownItem>
                            }

                            {(user.role ==='admin' &&
                                    <DropdownItem onClick={() => this.toggleDeleteModal(place._id)}>
                                        <p>Delete</p>
                                    </DropdownItem>
                            )}

                            <DropdownItem divider/>

                        </DropdownMenu>
                    </UncontrolledButtonDropdown>
                </div>

                <Modal isOpen={this.state.showDeleteModal} toggle={this.toggleDeleteModal}>
                    <ModalHeader toggle={this.toggleDeleteModal}>
                        <h3>Confirmation</h3></ModalHeader>
                    <ModalBody>
                        <p>You sure that you want to delete this place?</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={() => this.props.deletePlace(place._id)}>
                            <p>Yes</p>
                        </Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>
                            <p>Cancel</p>
                        </Button>
                    </ModalFooter>
                </Modal>

            </Fragment>
        );
    };
}

const mapDispatchToProps = dispatch => ({
    deletePlace: id => dispatch(deletePlace(id)),
});

export default  withRouter(connect(null, mapDispatchToProps)(EventDropdown));