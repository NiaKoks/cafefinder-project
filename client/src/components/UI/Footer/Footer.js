import React from 'react';
import {Nav, Navbar, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import AnonymousMenu from "../Toolbar/Menus/AnonymousMenu";


const Footer = ({user}) => {
  return (

        <Navbar color="dark" dark expand="md">
          <Nav className="ml-left" navbar>
            <NavItem>

                     <NavLink tag={RouterNavLink} to="/" exact>На главную</NavLink>

            </NavItem>
            {user ? null : <AnonymousMenu/>}
          </Nav>
        </Navbar>

  );
};

export default Footer;