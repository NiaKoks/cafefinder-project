import React, {Fragment} from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import AvatarThumbnail from "../../../AvatarThumbnail/AvatarThumbnail";


const UserMenu = ({user,place, logout}) => {
    return (
        <Fragment>
            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret className='d-inline-block'>
                    Привет, {user.displayName}!
                </DropdownToggle>
                <AvatarThumbnail user={user}/>
                <DropdownMenu right>
                    {/*console.log(place)*/}
                    {/*{place.owner ?(*/}
                    {/*    <DropdownItem tag={RouterNavLink}*/}
                    {/*                  to={"/places/" + place.owner._id}>*/}
                    {/*        My Places*/}
                    {/*    </DropdownItem>*/}
                    {/*) : null }*/}
                    <DropdownItem tag={RouterNavLink}
                                  to={"/places/create_new"}>
                        Create Place
                    </DropdownItem>
                    <DropdownItem divider/>
                    <DropdownItem onClick={logout}>
                        Logout
                    </DropdownItem>
                </DropdownMenu>
            </UncontrolledDropdown>
        </Fragment>
    )

};

export default UserMenu;
