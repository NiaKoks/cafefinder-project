import React from 'react';
import {
    Nav,
    Navbar,
    NavbarBrand,
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import UserMenu from "./Menus/UserMenu";
import AnonymousMenu from "./Menus/AnonymousMenu";
import logo from "../../../assets/images/logo.png";

const Toolbar = ({user, place,logout}) => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand tag={RouterNavLink} to="/"><img src={logo} alt='Logo' style={{height: '65px'}} /></NavbarBrand>
            <Nav className="ml-auto" navbar>
                {user ? <UserMenu user={user} logout={logout} place={place}/> : <AnonymousMenu/>}
            </Nav>
        </Navbar>
    );
};

export default Toolbar;