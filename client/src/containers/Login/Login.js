import React, {Component, Fragment} from 'react';
import {loginUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import Form from "reactstrap/es/Form";
import FormElement from "../../components/UI/Form/FormElement";
import {Alert, Button, Col, FormGroup} from "reactstrap";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Login extends Component {

    state = {
        email: '',
        password: '',
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.loginUser({...this.state})
    };

    render() {
        return (
            <Fragment>
                <h2>Авторизация</h2>
                {this.props.error &&  (
                    <Alert color="danger">{this.props.error.error || this.props.error.global}</Alert>
                )}
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="email"
                        title="Email"
                        type="email"
                        value={this.state.email}
                        onChange={this.inputChangeHandler}
                        required

                    />

                    <FormElement
                        propertyName="password"
                        title="Пароль"
                        type="password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        required

                    />

                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Войти
                            </Button>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <FacebookLogin/>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);