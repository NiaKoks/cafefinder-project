import React, {Component, Fragment} from 'react';
import Container from "reactstrap/es/Container";
import {connect} from "react-redux";
import {createPlace} from "../../store/actions/placesActions";
import CreatePlace from "../../components/CreatePlaceForm/CreatePlace";

class NewPlace extends Component {

    createPlace = placeData =>{
      this.props.onPlaceCreated(placeData).then(() =>{
         this.props.history.push('/')
      });
    };

    render() {
        return (
            <Fragment>
                <Container>
                    <h2>Add new place</h2>
                    <CreatePlace onSubmit={this.createPlace}/>
                </Container>
            </Fragment>
        );
    }
}
const mapDispatchToProps = dispatch =>({
   onPlaceCreated: placeData => dispatch(createPlace(placeData))
});
export default connect(null,mapDispatchToProps)(NewPlace);