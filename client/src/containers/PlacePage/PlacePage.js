import React, {Component,Fragment} from 'react';
import {connect} from "react-redux";
import {fetchSinglePlace} from "../../store/actions/placesActions";
import config from "../../config";

import {Button, Col, FormGroup, Input, InputGroup, InputGroupAddon, Label, Row} from "reactstrap";
import {deleteReview, fetchReviews, postReview} from "../../store/actions/reviewsActions";
import {deleteImage, fetchImages, postImage} from "../../store/actions/imagesActions";

import Rating from "react-rating";

import "./PlacePage.css"
import EventDropdown from "../../components/UI/EventDropdown";

class PlacePage extends Component {

    state = {
      text:'',
      foodRating: 1,
      serviceRating:1,
      interiorRating:1,
      image:''
    };

    componentDidMount() {
        this.props.fetchSinglePlace(this.props.match.params.id);
        this.props.fetchReviews(this.props.match.params.id);
        this.props.fetchImages(this.props.match.params.id);
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    postReview = event =>{
      event.preventDefault();
      this.props.postReview({
          ...this.state,
          place: this.props.match.params.id
      })
    };
    postImage = event =>{
        event.preventDefault();

        const imageData = new FormData();

        imageData.append("image",this.state.image);
        imageData.append("place",this.props.match.params.id);

        this.props.postImage(imageData);
    };
    render() {

        if (this.props.place === null ) return null;

        let avgFoodRating = 0;
        let avgInteriorRating = 0;
        let avgServiceRating = 0;
        let totalAverage = 0;

        if (this.props.reviews) {
            this.props.reviews.forEach(review =>{
                avgFoodRating += review.foodRating;
                avgInteriorRating += review.interiorRating;
                avgServiceRating += review.serviceRating;});

            avgFoodRating = avgFoodRating/this.props.reviews.length;
            avgInteriorRating = avgInteriorRating/this.props.reviews.length;
            avgServiceRating = avgServiceRating/this.props.reviews.length;
            totalAverage = (avgFoodRating + avgInteriorRating + avgServiceRating)/3;
        }

        return (
            <Fragment>
            <h2>{this.props.place.name}</h2>
                <img src={config.apiURL + '/uploads/' + this.props.place.logo}
                     alt="Place logo here" style={{width:'70%'}}/>
             <div>
                 <h4>About the place:</h4>
                 <p>{this.props.place.description}</p>
             </div>

                {!(isNaN(totalAverage)) &&
             <div>
                 <hr/>
                 <h4>Rating</h4>

                 <div>
                     <p>Average rating of place:</p>
                 <Rating readonly initialRating={totalAverage}/>
                     {totalAverage.toFixed(2)}
                 </div>
                 <div>
                     <p>Food:</p>
                 <Rating readonly initialRating={avgFoodRating}/>
                     {avgFoodRating.toFixed(2)}
                 </div>
                 <div>
                     <p>Service:</p>
                 <Rating readonly initialRating={avgServiceRating}/>
                     {avgServiceRating.toFixed(2)}
                 </div>
                 <div>
                     <p>Interior:</p>
                 <Rating readonly initialRating={avgInteriorRating}/>
                     {avgInteriorRating.toFixed(2)}
                 </div>
                 <hr/>
             </div>
             }
              {(this.props.images.length>0) &&
                  <div className="gallery">
                      <h4>Gallery:</h4>
                      {this.props.images.map(image =>(
                          <div style={{backgroundImage: 'url(' + config.apiURL + '/uploads/' + image.image + ')'}}>
                              {(this.props.user && this.props.user.role === 'admin') &&
                                  <Button
                                      color="danger"
                                      onClick={() => window.confirm('Do you really wanna delete this image?') && this.props.deleteImage(image._id,image.place)}
                                  >
                                      X
                                  </Button>
                              }
                          </div>
                      ))}
                  </div>
              }
                <div>
                    <h4>Reviews</h4>
                    <div>
                        {this.props.reviews.map(review =>(
                           <div key={review._id}>

                               {(this.props.user && this.props.user.role === 'admin') &&
                               <Button
                                   color="danger"
                                   onClick={() => window.confirm('Do you really wanna delete this review?') && this.props.deleteReview(review._id, review.place)}
                               >
                                   X
                               </Button>
                               }

                               <h5>{review.date.split('T')[0]}:  user {review.user.displayName} said:</h5>
                               <p>{review.text}</p>
                               <div>
                                   <p>Food:</p>
                                   <Rating readonly initialRating={review.foodRating}/>
                                   {review.foodRating.toFixed(2)}
                               </div>
                               <div>
                                   <p>Service:</p>
                                   <Rating readonly initialRating={review.serviceRating}/>
                                   {review.serviceRating.toFixed(2)}
                               </div>
                               <div>
                                   <p>Interior:</p>
                                   <Rating readonly initialRating={review.interiorRating}/>
                                   {review.interiorRating.toFixed(2)}
                               </div>
                           </div>
                        ))}
                    </div>
                    <hr/>
                    <h4>Add review</h4>
                    <div>
                        <Input type="textarea" name="text" id="text"
                               value={this.state.text} onChange={this.inputChangeHandler}
                        />

                        <FormGroup row>
                            <Label sm={2}><p>Rate the place:</p></Label>
                            <Col sm={10}>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">Food:</InputGroupAddon>
                                    <Input type="select" name="foodRating" id="foodRating"
                                           value={this.state.foodRating} onChange={this.inputChangeHandler}
                                    >
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input>
                                    <InputGroupAddon addonType="append">Service:</InputGroupAddon>
                                    <Input type="select" name="serviceRating" id="serviceRating"
                                           value={this.state.serviceRating} onChange={this.inputChangeHandler}
                                    >
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input>
                                    <InputGroupAddon addonType="append">Interior:</InputGroupAddon>
                                    <Input type="select"
                                        name="interiorRating" id="interiorRating" placeholder="max"
                                           value={this.state.interiorRating} onChange={this.inputChangeHandler}
                                    >
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Input>
                                </InputGroup>
                            </Col>
                        </FormGroup>
                        <Button color="success" onClick={this.postReview}> Left a review</Button>
                    </div>
                    <hr/>
                    <h4>Add photos for place</h4>
                    <div>
                        <FormGroup row>
                            <Label sm={2}>Select the file:</Label>
                            <Col sm={10}>
                                <Input type="file" name="image" onChange={this.fileChangeHandler}/>
                            </Col>
                        </FormGroup>
                        <Button color="success" onClick={this.postImage}>Post picture</Button>
                    </div>
                </div>
            </Fragment>
        );
    }
}
const mapStateToProps = state => ({
   place: state.places.singlePlace,
   user:state.users.user,
   reviews: state.reviews.reviews,
   images: state.images.images
});
const mapDispatchToProps = dispatch =>({
      fetchSinglePlace: (id) => dispatch(fetchSinglePlace(id)),
      fetchReviews: (id) => dispatch(fetchReviews(id)),
      fetchImages: (id) => dispatch(fetchImages(id)) ,
      postReview : (reviewData) => dispatch(postReview(reviewData)),
      postImage: (imageData) => dispatch(postImage(imageData)),
      deleteImage: (imageId,placeId) => dispatch(deleteImage(imageId,placeId)),
      deleteReview: (reviewId, placeId) => dispatch(deleteReview(reviewId, placeId)),
});

export default connect(mapStateToProps,mapDispatchToProps)(PlacePage);