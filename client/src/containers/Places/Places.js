import React, {Component,Fragment} from 'react';
import PlaceCard from "../../components/PlaceCard/PlaceCard";
import {connect} from "react-redux";
import {fetchPlaces} from "../../store/actions/placesActions";
import {Col, Row} from "reactstrap";

import "./Places.css"

class Places extends Component {

    componentDidMount() {
        this.props.onFetchPlaces();
    }

    render() {
        return (
            <div>
                <h2>Places</h2>
                <Fragment>
                    <Row  className="main-block">
                        <Col sm={10} className="place-cards">
                            {this.props.places.map(place =>(
                                <PlaceCard key ={place._id} place={place} user={this.props.user}/>
                            ))}
                        </Col>
                    </Row>
                </Fragment>
            </div>
        );
    }
}

const mapStateToProps = state =>({
   places: state.places.places,
   user:state.users.user,
});

const mapDispatchToProps = dispatch =>({
    onFetchPlaces: () => dispatch(fetchPlaces())
});

export default connect(mapStateToProps,mapDispatchToProps)(Places);