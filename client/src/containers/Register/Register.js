import React, {Component, Fragment} from 'react';
import {registerUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import Form from "reactstrap/es/Form";
import {Alert, Button, Col, FormGroup} from "reactstrap";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Register extends Component {
    state = {
        displayName: '',
        email: '',
        password: '',
        phone: '',
        avatar: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    submitFormHandler = (event) => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.registerUser(formData)
    };



    render() {
        return (
            <Fragment>
                <h2>Регистрация</h2>
                {this.props.error && !this.props.error.errors &&  (
                    <Alert color="danger">{this.props.error.error || this.props.error.global}</Alert>
                )}
                <Form onSubmit={this.submitFormHandler}>

                    <FormElement
                        propertyName="displayName"
                        title="Ваше имя"
                        type="text"
                        value={this.state.displayName}
                        onChange={this.inputChangeHandler}

                    />

                    <FormElement
                        propertyName="email"
                        title="Email"
                        type="text"
                        value={this.state.email}
                        onChange={this.inputChangeHandler}

                    />

                    <FormElement
                        propertyName="password"
                        title="Пароль"
                        type="password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}

                    />

                    <FormElement
                        propertyName="avatar"
                        title="Фото"
                        type="file"
                        onChange={this.fileChangeHandler}

                    />

                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Регистрация
                            </Button>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <FacebookLogin/>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);