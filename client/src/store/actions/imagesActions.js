import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {fetchPlaces} from "./placesActions";

export const FETCH_PLACE_IMAGES_SUCCESS = "FETCH_PLACE_IMAGES_SUCCESS";

export const fetchPlaceImagesSuccess = images =>({type:FETCH_PLACE_IMAGES_SUCCESS,images});

export const fetchImages = (placeId) => {
    return dispatch =>{
        return axios.get('/images?place='+ placeId).then(
            response => dispatch(fetchPlaceImagesSuccess(response.data))
        );
    }
};

export const postImage = (imageData) =>{
    return dispatch =>{
        return axios.post('/images',imageData).then(
            () => {
                NotificationManager.success(`You've successfully posted picture for place!`);
                dispatch(fetchImages(imageData.get('place')))
            }
        )
    }
};

export const deleteImage = (imageId,placeId) => {
    return (dispatch) => {
        return axios.delete(`/images/${imageId}`).then(
            () => {
                NotificationManager.success(`You've successfully deleted picture of the place!`);
                dispatch(fetchImages(placeId))
            }
        )
    }
};