import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";

export const FETCH_PLACES_SUCCESS = 'FETCH_PLACES_SUCCESS';
export const CREATE_PLACES_SUCCESS = 'CREATE_PLACES_SUCCESS';
export const FETCH_SINGLE_PLACE_SUCCESS ='FETCH_SINGLE_PLACE_SUCCESS';
export const FETCH_USER_PLACES_SUCCESS = 'FETCH_USER_PLACES_SUCCESS';

export const fetchPlacesSuccess = places => ({type:FETCH_PLACES_SUCCESS,places});
export const createPlacesSuccess = places =>({type: CREATE_PLACES_SUCCESS,places});
export const fetchSinglePlaceSuccess = (place,newPlace) =>({type: FETCH_SINGLE_PLACE_SUCCESS,place,newPlace});
export const fetchUserPlacesSuccess = places =>({type:FETCH_USER_PLACES_SUCCESS,places});

export const fetchPlaces = () => {
    return dispatch => {
        return axios.get('/places').then(
            response => dispatch(fetchPlacesSuccess(response.data))
        );
    };
};

export const fetchSinglePlace =(id)=>{
    return dispatch =>{
        return axios.get(`/places/${id}`).then(
            response => dispatch(fetchSinglePlaceSuccess(response.data))
        )
    };
};

export const fetchUserPlaces = () =>{
    return dispatch =>{
        return axios.get('/places/my').then(
            response =>dispatch(fetchUserPlacesSuccess(response.data))
        )
    };
};

export const createPlace = placeData =>{
    return dispatch =>{
        return axios.post('/places',placeData).then(()=>dispatch(createPlacesSuccess()));
    }
};

export const deletePlace = (placeId) => {
    return (dispatch) => {
        return axios.delete(`/places/${placeId}`).then(
            () => {
                NotificationManager.success(`You've successfully deleted place!`);
                dispatch(fetchPlaces())
            }
        )
    }
};