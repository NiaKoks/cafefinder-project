import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";

export const FETCH_PLACE_REVIEWS_SUCCESS = "FETCH_PLACE_REVIEWS__SUCCESS";

export const fetchPlaceReviewsSuccess = reviews =>({type:FETCH_PLACE_REVIEWS_SUCCESS,reviews});

export const fetchReviews = (placeId) => {
    return dispatch => {
        return axios.get('/reviews?place='+ placeId).then(
            response => dispatch(fetchPlaceReviewsSuccess(response.data))
        );
    };
};

export const postReview = (reviewData) =>{
    return dispatch =>{
        return axios.post('/reviews',reviewData).then(
            () => {
                NotificationManager.success(`You've successfully posted review to place!`);
                dispatch(fetchReviews(reviewData.place))
            }
        )
    }
};

export const deleteReview = (reviewId, placeId) => {
    return (dispatch) => {
        return axios.delete(`/reviews/${reviewId}`).then(
            () => {
                NotificationManager.success(`You've successfully deleted review!`);
                dispatch(fetchReviews(placeId))
            }
        )
    }
};