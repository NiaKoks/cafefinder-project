import {
    FETCH_PLACES_SUCCESS,FETCH_SINGLE_PLACE_SUCCESS,FETCH_USER_PLACES_SUCCESS
} from "../actions/placesActions";

const initialState = {
  places: [],
  userPlaces:[],
  singlePlace: null,
  singlePlaceNew: null,
};

const placesReducer = (state = initialState,action) =>{
  switch (action.type) {
      case FETCH_PLACES_SUCCESS:
          return {...state,places:action.places};
      case FETCH_SINGLE_PLACE_SUCCESS:
          return {...state,singlePlace: action.place,singlePlaceNew: action.newPlace};
      case FETCH_USER_PLACES_SUCCESS:
          return {...state,userPlaces: action.places}
      default:
          return state;
  }
};

export default placesReducer;