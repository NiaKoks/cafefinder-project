import { FETCH_PLACE_REVIEWS_SUCCESS} from "../actions/reviewsActions";

const initialState ={
  reviews:[],
};

const reviewsReducer = (state = initialState,action) =>{
  switch (action.type) {
      case FETCH_PLACE_REVIEWS_SUCCESS:
      return {...state,reviews: action.reviews}
      default:
          return state
  }
};

export default reviewsReducer;